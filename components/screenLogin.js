import { Root } from "native-base";
import React, { useState, useEffect, useRef } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
  } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Button, Toast, Fab, Footer, FooterTab, Icon, Spinner  } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { SocialIcon } from 'react-native-elements'

const styles = StyleSheet.create({
  center: {
    textAlign: 'center'
  }
});
function ScreenLogin({ route, navigation }) {
  const [spinner_status, setSpinner_status] = useState(0);

  const [userEmail, setUserEmail] = useState('');
  const [userPassword, setUserPassword] = useState('');

  async function handleLoginClick (e) {
    console.log('The link was clicked.');
    if (!userEmail) {
      alert('Please fill Email');
      return;
    }
    if (!userPassword) {
      alert('Please fill Password');
      return;
    }
    setSpinner_status(true);
    const rawResponse = await fetch('https://challenge.maniak.co/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        redirect: 'follow',
        body: JSON.stringify({username: userEmail, password: userPassword})
      }
    )
    .then(async response => {
      if(response.status==200){
        return await response.json()
      }else{
        throw `Error usuario y contraseña`;
      }
    }).then(async data => {
        //console.log(data.token)
        await AsyncStorage.setItem('token', data.token)
        return data.token
      }
    ).catch((exception) => {
      alert(exception)
    });
    console.log("rawResponse",rawResponse)
    navigation.navigate('Index')
  }
  useEffect(async() => {
    const token = await AsyncStorage.getItem('token')
    console.log("token: ",token)
    if(token!==null){
      navigation.navigate('Index')
    }
  }, []);
  if (__DEV__) {
    console.log("__DEV__","Modo desarrollador")
  }
  /*challenge@maniak.co*/
  /*maniak2020*/
  return (
    <Root>
      <Container>
        <Content>
          <Form>
              <Item>
                <Input placeholder="Username"
                                onChangeText={(UserEmail) =>
                                  setUserEmail(UserEmail)
                                }
                                autoCapitalize="none"
                                />
              </Item>
              <Item last>
                <Input placeholder="Password"
                       onChangeText={(UserPassword) =>
                         setUserPassword(UserPassword)
                       }
                       secureTextEntry={true}
                       />
              </Item>
              <Button full onPress={handleLoginClick}>
                <Text>log in</Text>
              </Button>
            </Form>
        </Content>
      </Container>
    </Root>
  );
}

export default ScreenLogin