import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    StatusBar,
  } from 'react-native';
import { Container, Header, Content, Form, Item, Title, Input, Label, Left, Right, Segment, Button, Fab, Footer, FooterTab, Icon, Card, CardItem, Body  } from 'native-base';
import Cards from "./cards";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { v4 as uuidv4 } from 'uuid';
import AsyncStorage from '@react-native-async-storage/async-storage';

function ScreenIndex({ route, navigation }) {
    const [rawResponse, setRawResponse] = useState(false);

    // De forma similar a componentDidMount y componentDidUpdate
    useEffect(async () => {
        const token = await AsyncStorage.getItem('token')
        console.log("token: ",token)
        await fetch('https://challenge.maniak.co/api/images', {
            method: 'GET',
            headers: {
              'Authorization': "Bearer "+ token
            }
          }
        )
        .then(response => {
          console.log("response:",response.status)
          if(response.status==200){
            return response.json()
          }else{
            throw `Error al cargar la informacion`;
          }
        }).then(data => {
            console.log("data: "+data)
            setRawResponse(data)
            //console.log("rawResponse",rawResponse)
            //return data
          }
        ).catch((exception) => {
          alert(exception)
        });
    },[]);
    if (!rawResponse) return (<><Header></Header><Text>'Loading...'</Text></>);
    return (
        <>
            <Header>
                <Left>
                    <Button
                        transparent
                        onPress={() => navigation.openDrawer()}>
                        <Icon type="Entypo" name="menu" />
                    </Button>
                </Left>
                <Body>
                    <Title>Data</Title>
                </Body>
                <Right />
            </Header>
            <Content>
            {
                rawResponse.map(function(item) {
                  console.log(item)
                  //return <Cards key={uuidv4()} item={item}/>
                  return <Cards item={item}/>
                })
            }
            </Content>
        </>
    )
}
export default ScreenIndex
