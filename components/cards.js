import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    StatusBar,
    Drawer
  } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Button, Fab, Footer, FooterTab, Icon  } from 'native-base';
import { Card, CardItem, Body } from 'native-base';


function Cards({ item, route, navigation }) {
    console.log(item)
    String.prototype.trimEllip = function (length) {
        return this.length > length ? this.substring(0, length) + "..." : this;
    }
    const styles = StyleSheet.create({
        titles: {
            fontWeight: '700'
        }
      });
    return(
            <Card>
                <CardItem header style={[styles.titles,{backgroundColor: "rgb(50, 145, 168)"}]} >
                    <Text style={styles.titles} >{item.title}</Text>
                </CardItem>
                <CardItem style={{backgroundColor: "rgb(50, 145, 168)"}} >
                    <Body>
                        <Text>
                            {item.description.trimEllip(100)}
                        </Text>
                        <Image source={{uri: item.image}}
       style={{width: 100, height: 100}} />
                    </Body>
                </CardItem>
            </Card>
    )
}
export default Cards