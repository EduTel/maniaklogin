import React, { useState, useEffect } from 'react';
import ScreenIndex from './screenIndex'
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import ScreenLogin from './screenLogin'
import App from '../App'
//import { NavigationActions } from 'react-navigation';
import { CommonActions } from '@react-navigation/native';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
  } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function MyDrawerNavigator({ navigation }) {
    const Drawer = createDrawerNavigator();
    const styles = StyleSheet.create({
        titles: {
            color: '#000',
            fontWeight: '700'
        }
    });
    function CustomDrawerContent(props) {
        return (
          <DrawerContentScrollView {...props}>
            <DrawerItemList {...props} />
            <DrawerItem label="logout" onPress={async () => {
                //client.resetStore()
                console.log("logout")
                await AsyncStorage.removeItem("token")
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            { name: 'Login' }
                        ],
                    })
                )
            }
            } />
          </DrawerContentScrollView>
        );
      }
    return (
        <Drawer.Navigator initialRouteName="Home" drawerStyle={{
            backgroundColor: '#fff'
          }}
          drawerContentOptions={{
            activeTintColor: '#00096f'
          }}
          drawerContent={props => <CustomDrawerContent {...props} />}
          >
          <Drawer.Screen name="Home" component={ScreenIndex} />
        </Drawer.Navigator>
    );
  }